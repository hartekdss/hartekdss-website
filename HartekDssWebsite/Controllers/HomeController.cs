﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.Mvc;
namespace HartekDSSWebsite.Controllers
{
    public class HomeController : Controller
    {


        public ActionResult Index()
        {
            return View();
        }

        //public ActionResult SiteUnderMaitnance()
        //{
        //    return View();
        //}
        [Route("about-us")]
        public ActionResult AboutUs()
        {
            return View();
        }
        [Route("career")]
        public ActionResult Career()
        {
            return View();
        }
        [Route("contact-us")]
        public ActionResult Contactus()
        {
            return View();
        }
        [Route("our-team")]
        public ActionResult Ourteam()
        {
            return View();
        }
        [Route("services")]
        public ActionResult Services()
        {
            return View();
        }
        [Route("why-us")]
        public ActionResult Whyus()
        {
            return View();
        }
        [Route("Home/SendMail")]
        [HttpPost]
        public int SendMail(contactForm contact)
        {

            try
            {

                string contactForm = "contact";
                string jsondata = JsonConvert.SerializeObject(contact);

             //   System.IO.File.AppendAllText(Server.MapPath("~/contact/" + contactForm + ".json"), jsondata + Environment.NewLine);





                List<string> emailTo = new List<string>();
              emailTo.Add("info@felizeek.com");
              //  emailTo.Add("CHANDRA.PRAKASH@felizeek.com");
                //string Message = string.Empty;


                //var networkCredential = new NetworkCredential
                //{
                //    Password = "8PfWAnzBF2LQhZs0",
                //    UserName = "chandra.prakash@felizeek.com"
                //};

                System.Globalization.TextInfo myTI = new System.Globalization.CultureInfo("en-US", false).TextInfo;

                MailMessage mail = new MailMessage();


                SmtpClient SmtpServer = new SmtpClient();
                mail.From = new MailAddress("info@felizeek.com", "Felizeek Support");


                foreach (var email in emailTo)
                {
                    mail.To.Add(email.ToString());

                }
                string message = "<b>Enquiry Details:-</b> \n\n<br><br>";
                message += "<b>Name:</b> " + contact.username + "\n\n<br>";
                message += "<b>Email: </b>" + contact.email + "\n\n<br>";
                message += "<b>Phone Number:</b> " + contact.phone + "\n\n<br>";
                message += "<b>Location:</b> " + contact.location + "\n\n<br>";
                message += "<b>Company Name:</b> " + contact.companyname + "\n\n<br>";
              //  message += "<b>Subject:</b> " + contact.subject + "\n\n<br>";
                message += "<b>Message:</b> " + contact.message + "\n\n<br>";

                mail.Subject = "Felizeek Enquiry: " + contact.companyname.Replace(" ", "_") + "_" + contact.username.Replace(" ", "_");
                mail.Body = message;
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;
                SmtpServer.TargetName = "Felizeek Enquiry";

                SmtpServer.EnableSsl = false;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Port = 25;

                SmtpServer.Host = "relay-hosting.secureserver.net";
                SmtpServer.Send(mail);

                // send mail to customer

                mail = new MailMessage();
                //string fullPath = "../template/thanksmail.html";
                StreamReader r = new StreamReader(Server.MapPath("~/template/thanksmail.html"), Encoding.GetEncoding("iso-8859-1"));

                message = r.ReadToEnd();

                mail.From = new MailAddress("info@felizeek.com", "Felizeek Support");


                 mail.To.Add(contact.email.ToString());
              //  mail.To.Add("chandra.prakash@felizeek.com");
                // mail.To.Add("info@hartekdss.com");
                mail.Subject = "Felizeek Support";
                mail.Body = message;
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;

                SmtpServer.EnableSsl = false;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Port = 25;
         
                SmtpServer.Host = "relay-hosting.secureserver.net";

                SmtpServer.Send(mail);

            }

            catch (Exception ex)
            {
                //throw ex;
                //  Console.WriteLine(ex.ToString());
               // System.IO.File.AppendAllText(Server.MapPath("~/contact/error.txt"), ex.ToString() + Environment.NewLine);

                return 1;
            }
            return 1;
        }

        [Route("partners")]
        public ActionResult partners()
        {
            return View();
        }
        public class contactForm
        {
            public string companyname { get; set; }
            public string username { get; set; }
            public string phone { get; set; }
            public string location { get; set; }
            public string email { get; set; }
            public string message { get; set; }
           // public string subject { get; set; }

        }

    }
}